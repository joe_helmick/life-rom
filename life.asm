; Conway's game of life by Joe Helmick (c) 2019
; https://gitlab.com/joe_helmick/life-rom
;
; Serial communications section by (c) Grant Searle
; http://searle.hostei.com/grant/#MyZ80
;
; Assembled using z88dk toolkit.
; https://www.z88dk.org/forum/
;
; note: set tab size to 8 for best alignment
; note: set terminal emulator to display code pagte CP437 for best appearance
;       or change charalive and chardead to regular ASCII characters
;
; This version of the game creates an "infinite" board, where the top and 
; bottom rows wrap around and the left and right columns wrap around also.
; This allows for long-lived patterns like gliders to just keep going 
; until they interact with other cells.  It's not so good for patterns like
; Gosper guns that spawn moving patterns, like gliders, as these gliders
; can wrap around and "attack" the established pattern, ruining it.

ramstart		=	8192		; first byte of ram; change only if memory changes
wwidth			=	60		; width of board
wheight			=	30		; height of board
charalive		=	254		; denotes filled cell on code page cp437
chardead		=	250		; denotes empty cell on code page cp437
;cpms			=	10000		; for 10 Mhz clock
cpms			=	8192		; for 8.192 Mhz clock (for delay routine)

;---------------------------------------------------------------------------------------------------
ser_bufsize		=	63			; serial buffer size, don't mess with this
ser_fullsize		=	ser_bufsize - 10	; serial fullsize, don't mess with this
ser_emptysize		=	5			; serial empty size, don't mess with this
;---------------------------------------------------------------------------------------------------
brdbufsize		=	wwidth * wheight; size of the board buffer
prntbufsize		=	7		; buffer length for number-printing routine
cr			=	13		; carriage return character
lf			=	10		; line feed character
cs			=	12		; form feed character, clear screen
bs			=	8		; backspace character
ctrlc			=	3		; control c, break character
bell			=	7		; bell character
rts_high		=	$d5		; 115200 bps from 68b50 acia w 1.8232 mhz clk
rts_low			=	$95		; 115200 bps from 68b50 acia w 1.8232 mhz clk
;---------------------------------------------------------------------------------------------------
; variables located in ram
defvars $2000
{
	serbuf			ds.w 1		; the start of the serial buffer
	serinptr		ds.w ser_bufsize; the buffer's input pointer
	serrdptr		ds.w 1		; the buffer's read pointer
	serbufused		ds.w 1		; count of bytes used
	
	generation		ds.w 1		; the life generation number
	population		ds.w 1		; the life current population
	brd1			ds.b brdbufsize	; the display board buffer
	brd2			ds.b brdbufsize	; the "working" board buffer
	rowloctbl1		ds.w wheight	; the row location table into field 1
	
	bufptr			ds.w 1		; the print buffer pointer
	curlen			ds.b 1		; the print current string length
	ngflag			ds.b 1		; the print negative flag
	buffer			ds.b prntbufsize; the print buffer size
	
	r_above			ds.b 1		; the index of the row above
	r_below			ds.b 1		; the index of the row below
	c_left			ds.b 1		; the index of the column to the left
	c_right			ds.b 1		; the index of the column to the right
	
	row			ds.b 1		; the current row
	col			ds.b 1		; the current column
	n_count			ds.b 1		; the neighbor count
	
	locbrd2			ds.w 1		; the address of the cell at the current row and column in board 2
	cellchar		ds.b 1		; character in the current cell
	
	lastvar			ds.b 1		; sentinel value for mfill routine, 
						; make sure it's always declared last
}

; ###################################################################################################
; ###########################  program execution start here at 0x0000 ###############################
; ###################################################################################################

;ORG of zero set in appmake command line, .ORG = 0x0000 there this first reset belongs.

hwreset:
	di			; turn off interrupts until started up
	jp	initcomms	; set up rs232
	
;---------------------------------------------------------------------------------------------------
	; defs	4, 0x00		; placeholder to align the vector to correct address
; reset0008:
	; jp	txa
	
;---------------------------------------------------------------------------------------------------
	; defs	5, 0x00		; placeholder to align the vector to correct address
; reset0010:
	; jp	rxa
	
;---------------------------------------------------------------------------------------------------
	; defs 	5, 0x00		; placeholder to align the vector to correct address
; reset0018:
	; jp	checkinputchar
	
;---------------------------------------------------------------------------------------------------
	; defs 	29, 0x00	; placeholder to align the vector to correct address
; reset0038:
	; jr	serialinterrupt
	
;---------------------------------------------------------------------------------------------------
; serialinterrupt:
	; push	af
	; push	hl
	; in	a, ($80)
	; and	$01			; is read buffer full?
	; jr	z, rts0		; ignore if not
	; in	a, ($81)		; read status
	; push	af
	; ld	a,(serbufused)
	; cp	ser_bufsize		; ignore if full
	; jr	nz, notfull
	; pop	af
	; jr	rts0
; notfull:
	; ld	hl,(serinptr)
	; inc	hl
	; ld	a,l
	; cp	(serbuf+ser_bufsize) & $ff
	; jr	 nz,notwrap
	; ld	 hl,serbuf
; notwrap:
	; ld	(serinptr),hl
	; pop	af
	; ld	(hl),a
	; ld	a,(serbufused)
	; inc	a
	; ld	(serbufused),a
	; cp	ser_fullsize
	; jr	c,rts0
	; ld	a,rts_high
	; out	($80),a
; rts0:
	; pop	hl
	; pop	af
	; ei
	; reti
	
;---------------------------------------------------------------------------------------------------
; rxa:
	; ld	a,(serbufused)
	; cp	$00
	; jr	z, rxa
	; push	hl
	; ld	hl,(serrdptr)
	; inc	hl
	; ld	a,l
	; cp	(serbuf+ser_bufsize) & $ff
	; jr	nz, notrdwrap
	; ld	hl,serbuf
; notrdwrap:
	; di
	; ld	(serrdptr), hl
	; ld	a, (serbufused)
	; dec	a
	; ld	(serbufused), a
	; cp	ser_emptysize
	; jr	nc, rts1
	; ld	a, rts_low
	; out	($80), a
; rts1:
	; ld	a, (hl)
	; ei
	; pop	hl
	; ret			; char ready in a
	
;---------------------------------------------------------------------------------------------------
txa:
	push	af		; store char in a for later
txaloop:
	in	a, ($80)	; read acia status byte       
	bit	1, a		; set z flag if still transmitting character       
	jr	z, txaloop	; and loop until flag signals ready
	pop	af		; get the character back from the stack
	out	($81), a	; send it over rs232 to acia outpout address
	ret
	
;---------------------------------------------------------------------------------------------------
; checkinputchar:
	; ld	a, (serbufused)
	; cp	$0
	; ret
	
;---------------------------------------------------------------------------------------------------
initcomms:
	ld	hl, serbuf	; set pointer to serial buffer
	ld	(serinptr), hl	; set tx pointer to same location
	ld	(serrdptr), hl	; set rx pointer to same location
	xor	a		; set a to zero
	ld	(serbufused), a	; set buffer used amount to zero
	ld	a, rts_low	; load the acia configuration byte...
	out	($80), a	; and initialize the acia with it
	im	1		; set interrupt mode...
	ei			; and enable interrupts
	jp	main1		; jump to start of program


	
;###################################################################################################
;#######################     end of initialization routines, start program      ####################
;###################################################################################################
;------------------------------------------------------------------------------------------
; String allocations.
generationmsg:	defm cs,"generation: ", 0
populationmsg:	defm "population: ", 0

;------------------------------------------------------------------------------------------
; Print a null-terminated string character by character wherever the cursor is.
; Address to string is in HL.
print:
	ld	a,(hl)
	or	a
	ret	z
	call	txa
	inc	hl
	jr	print
	ret
	
;---------------------------------------------------------------------------------------------------
; Copy one board to another.
; Source address is in HL
; Destination address is in DE
; Number of bytes to move is in BC.
; From "Z80 Assembly Language Subroutines" 1983 p. 199
copyboard:
	ld	a, b		; return immediately if BC is zero
	or	c
	ret	z
	
	ex	de, hl		; calculate destination - source
	push	hl		; save destination
	and	a		; clear carry
	sbc	hl, de
	and	a		; then subtract area size
	sbc	hl, bc
	pop	hl		; restore destination
	ex	de, hl
	jr	nc, doleft	; jump if no overlap between ranges
	
	add	hl, bc		; source = source + length - 1
	dec	hl
	ex	de, hl		; destination = destination + length - 1
	add	hl, bc
	dec	hl
	ex	de, hl
	lddr			; block move high to low
	ret
doleft:	
	ldir			; block move low to high
	ret
	
;---------------------------------------------------------------------------------------------------
; Display the board character at a time.  Add CRLF breaks at the end
; of every row to get proper rectangular appearance.
showboard:
; HL has address of board to paint
	ld	c, wheight
nextrow2:
	ld	b, wwidth
nextpos2:	
	ld	a, (hl)		; get the character
	call	txa		; transmit it
	inc	hl		; move to next address
	dec	b		; move to previous column
	ld	a, b				
	cp	0		; are we at beginning of row?
	jp	nz, nextpos2	; no? then continue 
	ld	a, cr		; yes? then add a CRLF sequence 
	call	txa
	ld	a, lf
	call	txa
	dec	c		; now decrement the row
	ld	a, c
	cp	0		; done?
	jp	 nz, nextrow2	; no? then do next row
	ret
	
;---------------------------------------------------------------------------------------------------
; Convert signed binary 16-bit number to decimal ascii
; From "Z80 Assembly Language Subroutines" 1983 p. 178 ff.	
bn2dec:
	ld	a, 0
	ld	(buffer + 0), a
	ld	(buffer + 1), a
	ld	(buffer + 2), a
	ld	(buffer + 3), a
	ld	(buffer + 4), a
	ld	(buffer + 5), a
	ld	(buffer + 6), a

	ld	(bufptr), hl
	ex	de, hl
	ld	a, 0
	ld	(curlen), a
	ld	a, h
	ld	(ngflag), a
	or	a
	jp	p, cnvert
	ex	de, hl
	ld	hl, 0
	or	a
	sbc	hl, de
cnvert:
	ld	e, 0
	ld	b, 16
	or	a
dvloop:
	rl	l
	rl	h
	rl	e
	ld	a, e
	sub	10
	ccf
	jr	nc, deccnt
	ld	e, a
deccnt:
	djnz	dvloop
	rl	l
	rl	h
chins:
	ld	a, e
	add	a, '0'
	call	insert
	ld	a, h
	or	l
	jr	nz, cnvert
exit:
	ld	a, (ngflag)
	or	a
	jp	p, pos
	ld	a, '-'
	call	insert
	ret
pos:	
	ret
insert:	
	push	hl
	push	af
	ld	hl, (bufptr)
	ld	d, h
	ld	e, l
	inc	de
	ld	(bufptr), de
	ld	a, (curlen)
	or	a
	jr	z, exitmr
	ld	c, a
	ld	b, 0
	lddr
exitmr:
	ld	a, (curlen)
	inc	a
	ld	(curlen), a
	ld	(hl), a
	ex	de, hl
	pop	af
	ld	(hl), a
	pop	hl
	ret
	
;---------------------------------------------------------------------------------------------------
; mfill
; From "Z80 Assembly Language Subroutines" 1983 p. 196
mfill:
	ld	(hl), a
	ld	d, h
	ld	e, l
	inc	de
	dec	bc
	ld	a, b
	or	c
	ret	z
	ldir
	ret
	
;---------------------------------------------------------------------------------------------------
; delay routine
; From "Z80 Assembly Language Subroutines" 1983
delay:
	push	bc
	call	dly
	ld	b, +(cpms/50)-2
ldlp:	jp	ldly1
ldly1:	jp	ldly2
ldly2:	jp	ldly3
ldly3:	add	a, 0
	djnz	ldlp
	pop	bc
	ld	a, (delay)
	ret
dly:	dec	a
	ret	z
	ld	b, +(cpms/50)-1
dlp:	jp	dly1
dly1:	jp	dly2
dly2:	jp	dly3
dly3:	add	a, 0
	djnz	dlp
	jp	dly4
dly4:	jp	dly5
dly5:	nop
	jp	 dly
	
;---------------------------------------------------------------------------------------------------
; Create row multipler table.
; Since the Z80 has no multiplication instruction, we construct here a table
; of row offsets based on the width and height of the table.  Result is a 
; memory lookup table with one entry per row, holding the address of that
; row on the main playing board.  This is a central routine in the program.
; Adapted from http://cpctech.cpc-live.com/docs/mult.html
createmulttable1:
	ld	ix, rowloctbl1
	ld	hl, 0
	ld	de, wwidth
	ld	b, wheight
tbl1:
	ld	(ix + 0), l
	ld	(ix + 1), h
	inc	ix
	inc	ix
	add	hl, de
	djnz	tbl1
	
;---------------------------------------------------------------------------------------------------
; Get the current character at the designated row-column position.
; This is a central routine in the program.
; input B holds column number
; input C holds row number
; input HL holds base address of row lookup table
; output A is saved in memory
getcurcharbrd:
	; Comments below illustrate use for made-up addresses

	; brd1:
	; byte addresses:
	; $2000 $2001 $2002 $2003 $2004 $2005 $2006 $2007
	;   .     .     x     .     x     .     .     .
	; $2008 $2009 $200a $200b $200c $200d $200e $200f
	;   .     .     x     .     x     .     x     .
	; $2010 $2011 $2012 $2013 $2014 $2015 $2016 $2017
	;   .     .     x     .     .    [z]    .     .
	; $2018 $2019 $201a $201b $201c $201d $201e $201f
	;   x     x     x     .     x     .     x     x

	; rowloctbl1
	; word addresses:
	; $3000 $3002 $3004 $3006 $3008 $300a $300c $300e
	; contents:
	; $2000 $2008 $2010 $2018 $2020 $2028 $2030 $2038

	; example b = 5
	;         c = 2

	push	hl
	push	de
	push	af

	; find proper row first
	ld	l, c		; l = 5
	ld	h, 0		; h = 0, hl = 0006
	add	hl, hl		; hl = 10
	ld	de, rowloctbl1	; de = $3000
	add	hl, de		; hl = $300A
	
	; get what it points to
	ld	a, (hl)		; a = 28
	ld	e, a		; e = 28
	inc	hl		; hl = $300B
	ld	 a, (hl)	; a = 20
	ld	d, a		; de = 20
	ex	de, hl		; de = 2028

	; add the column offset
	ld	d, 0				
	ld	e, b		; de = $0005
	add	hl, de		; add to HL
	ld	de, brd1
	add	hl, de		; add to board base address for result
	ld	a, (hl)		; get the character at that address		

	ld	(cellchar), a	; save it for later

	pop	af
	pop	de
	pop	hl
	ret
	
;---------------------------------------------------------------------------------------------------
; Do one LIFE generation here.  This is called by the main loop 
; once per generation.  Here we examine each cell and apply the 
; rules of LIFE, populating the "working" board in memory from
; the main board which is displayed.
; Some of the comments relate this implementation to python statements
; from a python LIFE program I wrote to test the algorithm.
dogeneration:
; reset the population count, accrue it as we figure out the next generation
	ld	hl, 0
	ld	(population), hl
; start at bottom and count down toward top of board row-major-wise
	ld	c, wheight - 1
	ld	hl, brd1	; start at this memory address
; start at right and count toward left	
nextrowa:	
	ld	b, wwidth - 1
	
; load the row from register c and determine "default" row above and row below
	ld	a, c
	ld	(row), a
	dec	a
	ld	(r_above), a
	inc	a
	inc	a
	ld	(r_below), a
	
edge_test_1:	
	ld	a, (row)	; if r == 0, r_above = height - 1
	cp	0
	jr	nz, edge_test_2
	ld	a, wheight - 1
	ld	(r_above), a
edge_test_2:	
	ld	a, (row)	; if r == height - 1, r_below = 0
	cp	wheight - 1
	jr	nz, edge_test_row_done
	ld	a, 0
	ld	(r_below), a
edge_test_row_done:	
	
	
nextposa:	
; load the column from register b and determine the default column to the left and right of it	
	ld	a, b
	ld	(col), a
	dec	a
	ld	(c_left), a
	inc	a
	inc 	a
	ld	(c_right), a
; now edge tests, which provide for "wrap-around" both top and bottom.  for example, row
; zero has no row above it, so we make its r_above value equal to the bottom row.  same
; for columns, left and right.  	
edge_test_3:	
	ld	a, (col)	; if c == 0, c_left = width - 1
	cp	0
	jp	nz, edge_test_4
	ld	a, wwidth - 1
	ld	(c_left), a
edge_test_4:
	ld	a, (col)	; if c == width -1, c_right = 0
	cp	wwidth - 1
	jp	nz, edge_tests_done
	ld	a, 0
	ld	(c_right), a

edge_tests_done:
; reset the neighbor count because we're going to start counting now
	ld 	a, 0
	ld 	(n_count), a	; n_count = 0
	
; count neighbors	
n_count_1:
	ld	a, (r_above)	; loc = (c_n * width) + r_n
	ld	c, a
	ld	a, (col)
	ld	b, a
	ld	hl, rowloctbl1
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_2
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_2:	
	ld	a, (r_above)	; loc = (c_ne * width) + r_ne
	ld	c, a
	ld	a, (c_right)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_3
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_3:
	ld	a, (row)	; loc = (c_e * width) + r_e
	ld	c, a
	ld	a, (c_right)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_4
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_4:		
	ld	a, (r_below)	; loc = (c_se * width) + r_se
	ld	c, a
	ld	a, (c_right)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_5
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_5:
	ld	a, (r_below)	; loc = (c_s * width) + r_s
	ld	c, a
	ld	a, (col)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_6
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_6:
	ld	a, (r_below)	; loc = (c_sw * width) + r_sw
	ld	c, a
	ld	a, (c_left)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_7
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_7:
	ld	a, (row)	; loc = (c_w * width) + r_w
	ld	c, a
	ld	a, (c_left)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, n_count_8
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a
n_count_8:	
	ld	a, (r_above)	; loc = (c_nw * width) + r_nw
	ld	c, a
	ld	a, (c_left)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	jp	nz, check_cur_cell
	ld	a, (n_count)
	inc	a		; n_count += 1
	ld	(n_count), a

; check whether current cell is dead or alive, as that is important in deciding which
; path to take	
check_cur_cell:
	ld	a, (row)	; loc = (c * width) + r
	ld	c, a
	ld	a, (col)
	ld	b, a
	call	getcurcharbrd
	ld	a, (cellchar)
	cp	charalive	; if brd1[loc] == mark_char
	 
	jp	nz, cell_dead	; jump to "cell is dead" routine
				; else continue with "cell is alive" code
	
cell_alive:
	ld	a, (n_count)	; load the neighbor count to test
test_for_0_neighbors:
	cp	0
	jp	nz, test_for_1_neighbors
	jp	mark_board_2_dead	; alive but lonely with 0 neighbors
test_for_1_neighbors:
	cp	1
	jp	nz, test_for_2_neighbors
	jp	mark_board_2_dead	; alive but lonely with 1 neighbor
test_for_2_neighbors:	
	cp	2
	jp	nz, test_for_3_neighbors
	jp	mark_board_2_alive	; alive and survives with 2 neighbors
test_for_3_neighbors:
	cp	3
	jp	nz, test_more_neighbors
	jp	mark_board_2_alive	; alive and survives with 3 neighbors
test_more_neighbors:
	jp	mark_board_2_dead	; 4 or more is overcrowded, cell dies
;-------------------------------------------------------
cell_dead:	
	ld	a, (n_count)	; load the neighbor count to test
	cp	3
	jp	z, mark_board_2_alive	; spawn new alive cell if has three neighbors
	jp	mark_board_2_dead  	; otherwise leave dead
;-------------------------------------------------------
; Same logic here as in getcurcharbrd but marking the char as alive.
mark_board_2_alive:
	ld	l, c
	ld	h, 0
	add	hl, hl
	ld	de, rowloctbl1
	add	hl, de
	ld	a, (hl)
	ld	e, a
	inc	hl
	ld	a, (hl)
	ld 	d, a
	ex	de, hl
	ld	d, 0
	ld 	e, b
	add	hl, de
	ld	de, brd2
	add	hl, de
	ld	(hl), charalive
	
	ld	hl, (population)	; increment and save population
	inc	hl
	ld 	(population), hl
	
	jr	next_position
;-------------------------------------------------------
; Same logic here as in getcurcharbrd but marking the char as dead.
mark_board_2_dead:	
	ld	l, c
	ld	h, 0
	add	hl, hl
	ld	de, rowloctbl1
	add	hl, de
	ld	a, (hl)
	ld	e, a
	inc	hl
	ld	a, (hl)
	ld 	d, a
	ex	de, hl
	ld	d, 0
	ld 	e, b
	add	hl, de
	ld	de, brd2
	add	hl, de
	ld	(hl), chardead
	jr	next_position
;-------------------------------------------------------
next_position:
	inc	hl				; next position
	dec	b				; count down toward zero left on this row
	ld	a, b		
	cp	$ff
	jp	nz, nextposa	; if not zero, repeat at next position on this row
	dec	c				; done with row, so move to next column
	ld	a, c
	cp	$ff
	jp	nz, nextrowa
	ret

        
;---------------------------------------------------------------------------------------------------
; load_pattern:
; ; simple test pattern, a single glider in an 8x8 space
; ; runs forever because it's the only life on the board
; ; and the board wraps around.
	; ld	a, charalive
	; ld	(brd1 + 18), a
	; ld	(brd1 + 27), a
	; ld	(brd1 + 33), a
	; ld	(brd1 + 34), a
	; ld	(brd1 + 35), a
        ; ret

;---------------------------------------------------------------------------------------------------
load_pattern:
; two gliders and two togglers in a 60 x 30 board
; starts off a nice 1000+ generation run
;000............................................................
;060...x........................................................
;120....xx......................................................
;180...xx.................x.....................................
;240......................x.....................................
;300......................x.................................x...
;360.............................xxx......................xx....
;420.......................................................xx...
	ld	a, charalive
	ld	(brd1 + 63), a
	ld	(brd1 + 124 ), a
	ld	(brd1 + 125), a
	ld	(brd1 + 183), a
	ld	(brd1 + 184), a
	
	ld	(brd1 + 202), a
	ld	(brd1 + 262), a
	ld	(brd1 + 322), a

	ld	(brd1 + 389), a
	ld	(brd1 + 390), a
	ld	(brd1 + 391), a
	
	ld	(brd1 + 356), a
	ld	(brd1 + 414), a
	ld	(brd1 + 415), a
	ld	(brd1 + 475), a
	ld	(brd1 + 476), a
        ret

;---------------------------------------------------------------------------------------------------
; load_pattern:
; ;                 1         2         3         4         5         6         7
; ;       01234567890123456789012345678901234567890123456789012345678901234567890123456789

; ;0000   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;0080   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;0160   ,......X..,.........,.........,.........,.........,.........,.........,.........
; ;0240   ,....X.X..,.........,.........,.........,.........,.........,.........,.........
; ;0320   ,.....XX..,.........,.........,.........,.........,.........,.........,.........
; ;0400   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;0480   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;0560   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;0640   ,.........,.........,.........,..XXX.X..,.........,.........,.........,.........
; ;0720   ,.........,.........,.........,..X......,.........,.........,.........,.........
; ;0800   ,.........,.........,.........,.....XX..,.........,.........,.........,.........
; ;0880   ,.........,.........,.........,...XX.X..,.........,.........,.........,.........
; ;0960   ,.........,.........,.........,..X.X.X..,.........,.........,.........,.........
; ;1040   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1120   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1200   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1280   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1360   ,.........,.........,.........,.........,.........,.........,.........,..X.X....
; ;1440   ,.........,.........,.........,.........,.........,.........,.........,..XX.....
; ;1520   ,.........,.........,.........,.........,.........,.........,.........,...X.....
; ;1600   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1680   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1760   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1840   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;1920   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2000   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2080   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2160   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2240   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2320   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2400   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2480   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2560   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2640   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2720   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2800   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2880   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;2960   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3040   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3120   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3200   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3280   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3360   ,.........,.........,.........,..........,.........,.........,.........,.........
; ;3440   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3520   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3600   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3680   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3760   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3840   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;3920   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4000   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4080   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4160   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4240   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4320   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4400   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4480   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4560   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4640   ,.........,.........,.........,.........,.........,.........,.........,.........
; ;4720   ,.........,.........,.........,.........,.........,.........,.........,.........
        
	; ld	a, charalive
        ; ld      (brd1 + 167 ), a
        ; ld      (brd1 + 245 ), a
        ; ld      (brd1 + 247 ), a
        ; ld      (brd1 + 326 ), a
        ; ld      (brd1 + 327 ), a
        
        ; ld      (brd1 + 640+ 33), a
        ; ld      (brd1 + 640+34), a
        ; ld      (brd1 + 640+35), a
        ; ld      (brd1 + 640+37), a
        ; ld      (brd1 + 720 + 33), a
        ; ld      (brd1 + 800+36), a
        ; ld      (brd1 + 800+37), a
        ; ld      (brd1 + 880+34), a
        ; ld      (brd1 + 880+35), a
        ; ld      (brd1 + 880+37), a
        ; ld      (brd1 + 960+33), a
        ; ld      (brd1 + 960+35), a
        ; ld      (brd1 + 960+37), a
	
        ; ld      (brd1 + 1360+73), a
        ; ld      (brd1 + 1360+75), a
        ; ld      (brd1 + 1440+73), a
        ; ld      (brd1 + 1440+74), a
        ; ld      (brd1 + 1520+74), a
	
	
        ; ret
        
	
;######################################################################################################
;###########     main endless loop starts here      ###################################################
;######################################################################################################

main1:
; load the stack pointer at top of ram
	ld	sp, $ffff

; null out all ram variables	
	ld	hl, ramstart
	ld	bc, lastvar - ramstart
	ld	a, 0
	call	mfill
; create the row location table
	call	createmulttable1
	
; initialize board with empty character	
	ld	hl, brd1
	ld	bc, brdbufsize
	ld	a, chardead
	call	mfill

; load pattern from above here
        call load_pattern
	
; this is the top of the "while true" loop	
whiletrueloop:	

	; print generation message and count
	ld hl, generationmsg
	call print
	ld de, (generation)
	ld hl, buffer
	call bn2dec
	ld hl, buffer		
	call print

	; print a CRLF
	ld a, cr
	call txa
	ld a, lf
	call txa

	; display main board
	ld	hl, brd1
	call	showboard

	; print population message and count
	ld	hl, populationmsg
	call	print
	ld	de, (population)
	ld	hl, buffer
	call	bn2dec
	ld	hl, buffer
	call	print

	; print a CRLF
	ld a, cr
	call txa
	ld a, lf
	call txa
	
	; save everything before doing main routine
	push	af
	push	bc
	push	de
	push	hl

	; do the main life routine for this generation
	call 	dogeneration

	; restore everything
	pop	hl
	pop	de
	pop	bc
	pop	af

	; copy board 2 back to board 1 to show new generation
	ld	hl, brd2
	ld	de, brd1
	ld	bc, brdbufsize
	call copyboard
	
	; increment the generation count and save it
	ld	de, (generation)
	inc	de
	ld	(generation), de
	
	; Delay a little bit, particularly important for small boards
	; to slow things down so you can see the patterns.
	ld	b, 2
msec100:
	ld	a, 100
	call	delay
	djnz	msec100

	; jump back to beginning of while true loop top	
	jp	whiletrueloop

	.end