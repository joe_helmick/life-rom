ECHO OFF
if exist *.bin ( del *.bin )
ECHO ASSEMBLING
D:\devtools\z88dk\z80asm.exe  --cpu=z80 --list --make-bin life.asm
ECHO MAKING
D:\devtools\z88dk\appmake.exe +hex --binfile life.bin --org 0000h
ECHO OFF
if exist *.o ( del *.o )
if exist *.err ( del *.err )
if exist *.ihx ( del *.ihx )